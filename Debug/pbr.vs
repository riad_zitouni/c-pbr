#version 330 core
layout (location = 0) in vec3 l_position;
layout (location = 1) in vec3 l_normal;
layout (location = 2) in vec2 l_uv;
layout (location = 3) in vec3 l_tangent;

out VertexData
{
	mat3 TBN;
}io_vd;

out vec3 io_normal;
out vec3 in_position;
out vec2 io_texCoords;

uniform mat4 u_modelMatrix;
uniform mat4 u_projectionMatrix;
uniform mat4 u_viewMatrix;

void main()
{

	gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vec4(l_position, 1.0f);
	
	in_position = (u_modelMatrix * vec4(l_position, 1.0)).xyz;

	io_normal = mat3(u_modelMatrix) * l_normal;
	io_normal = normalize(io_normal);

	vec3 tangent = (u_modelMatrix * vec4(l_tangent, 0.0)).xyz;
	tangent = normalize(tangent);

	io_texCoords = vec2(l_uv.x, l_uv.y);
    
	vec3 T = tangent;
	vec3 N = io_normal;
	vec3 B = normalize(cross(N, T));

	io_vd.TBN = mat3(T, B, N);
}
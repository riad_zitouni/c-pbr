#version 330 core
layout (location = 0) in vec3 l_position;

uniform mat4 u_projectionMatrix;
uniform mat4 u_viewMatrix;

out vec3 io_position;

void main()
{
    io_position = l_position;

	// Removes translation part from the matrix to make env map appear infinitely far away
	mat4 viewMatrix = mat4(mat3(u_viewMatrix));
	vec4 projection = u_projectionMatrix * viewMatrix * vec4(io_position, 1.0);

	// Ensures that depth of the cube is always 1 so that it's always behind other objects
	gl_Position = projection.xyww;
}
#version 330 core
layout (location = 0) in vec3 l_position;
layout (location = 1) in vec2 l_texCoords;

out vec2 io_texCoords;

void main()
{
    io_texCoords = l_texCoords;
	gl_Position = vec4(l_position, 1.0);
}
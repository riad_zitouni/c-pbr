#version 330 core

out vec4 fragmentColor;

in vec3 io_position;

uniform samplerCube u_environmentMap;

const float PI = 3.14159265359;

void main()
{
	vec3 normal = normalize(io_position);

	vec3 irradiance = vec3(0.0);

	vec3 up = vec3(0.0, 1.0, 0.0);
	vec3 right = cross(up, normal);
	up = cross(normal, right);

	float delta = 0.025f;
	float samples = 0.0f;
	for(float phi = 0.0; phi < 2.0 * PI; phi += delta)
	{
		for(float theta = 0.0; theta < 0.5 * PI; theta += delta)
		{
			// Convert from spherical to cartesian coordinates
			//using:
			// x = ro * sin(theta) * cos (phi)
			// y = ro * sin(theta) * sin (phi)
			// z = ro * cos(theta)
			vec3 sampleCartesian = vec3(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));

			// Convert to world coordinates
			vec3 sampleWorld = sampleCartesian.x * right + sampleCartesian.y * up + sampleCartesian.z * normal;

			// Integral of one sample
			irradiance += texture(u_environmentMap, sampleWorld).rgb * cos(theta) * sin(theta);
			samples++;
		}
	}

	irradiance = PI * irradiance * (1.0 / float(samples));

	fragmentColor = vec4(irradiance, 1.0);
}
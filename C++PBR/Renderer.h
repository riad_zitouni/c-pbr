// Author: Riad Zitouni

#ifndef RENDERER_H
#define RENDERER_H

#include "RenderShader.h"
#include "Scene.h"

namespace pbrcpp
{

#define VERTEX_SIZE sizeof(Vertex)

#define SHADER_POSITION_LOCATION 0 
#define SHADER_NORMAL_LOCATION 1

#define SHADER_UV_LOCATION 2

#define SHADER_TANGENT_LOCATION 3

#define MAX_TEXTURES 32

	class Renderer
	{
	public:
		void load();
		void draw(RenderShader* shader);
	};
}

#endif
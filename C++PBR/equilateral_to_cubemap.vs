#version 330 core

layout (location = 0) in vec3 l_position;

out vec3 io_position;

uniform mat4 u_projectionMatrix;
uniform mat4 u_viewMatrix;

void main(){
	io_position = l_position;
	gl_Position = u_projectionMatrix * u_viewMatrix * vec4(l_position, 1.0);
}

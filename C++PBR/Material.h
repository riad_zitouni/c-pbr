// Author: Riad Zitouni

#ifndef MATERIAL_H
#define MATERIAL_H

#include "Texture.h"

#include <glm\glm.hpp>

namespace pbrcpp
{
	class Material
	{
	public:
		Material();
		Material(glm::vec3 albedo, float roughness = 0.5, float metallic = 0.2, float ambientOcclusion = 1.0);
		Material(Texture albedo, Texture normal, Texture roughness, Texture metallic, Texture ambientOcclusion);

		inline const Texture getAlbedoMap() const { return m_albedoMap; }
		inline const Texture getNormalMap() const { return m_normalMap; }
		inline const Texture getRoughnessMap() const { return m_roughnessMap; }
		inline const Texture getMetallicMap() const { return m_metallicMap; }
		inline const Texture getAmbientOcclusionMap() const { return m_ambientOcclusionMap; }

		inline const glm::vec3 getAlbedo() const { return m_albedo; }
		inline const float getRoughness() const { return m_roughness; }
		inline const float getMetallic() const { return m_metallic; }
		inline const float getAmbientOcclusion() const { return m_ambientOcclusion; }

		inline void setMaterial(const glm::vec3& albedo, const float roughness, const float metallic, const float ao)
		{
			m_albedo = albedo;
			m_roughness = roughness;
			m_metallic = metallic;
			m_ambientOcclusion = ao;
		}

		inline void setAlbedo(const glm::vec3& albedo) { m_albedo = albedo; }
		inline void setRoughness(const float roughness) { m_roughness = roughness; }
		inline void setMetallic(const float metallic) { m_metallic = metallic; }
		inline void setAmbientOcclusion(const float ao) { m_ambientOcclusion = ao; }

	private:
		glm::vec3 m_albedo;
		float m_roughness;
		float m_metallic;
		float m_ambientOcclusion;

		Texture m_albedoMap;
		Texture m_normalMap;
		Texture m_roughnessMap;
		Texture m_metallicMap;
		Texture m_ambientOcclusionMap;
	};
}

#endif
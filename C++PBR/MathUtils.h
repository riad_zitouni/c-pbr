// Author: Riad Zitouni

#ifndef MATH_UTILS_H
#define MATH_UTILS_H

#include "Mesh.h"
#include "Camera.h"

#include <glm\glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>

namespace pbrcpp{

	static class MathUtils
	{
	public:
		static void printVector(glm::vec2 vector, const char* text = "")
		{
			std::cout << text << ": " << vector.x << " " << vector.y << " " << std::endl;
		}

		static void printVector(glm::vec3 vector, const char* text = "")
		{
			std::cout << text << ": " << vector.x << " " << vector.y << " " << vector.z << " " << std::endl;
		}

		static void printVector(const glm::vec4& vector, const char* text = "")
		{
			std::cout << text << ": " << vector.x << " " << vector.y << " " << vector.z << " " << vector.z << std::endl;
		}

		static void printMat4(const glm::mat4& m)
		{
			std::cout << "[ " << m[0][0] << "  " << m[1][0] << "  " << m[2][0] << "  " << m[3][0] << " ]" << std::endl;
			std::cout << "[ " << m[0][1] << "  " << m[1][1] << "  " << m[2][1] << "  " << m[3][1] << " ]" << std::endl;
			std::cout << "[ " << m[0][2] << "  " << m[1][2] << "  " << m[2][2] << "  " << m[3][2] << " ]" << std::endl;
			std::cout << "[ " << m[0][3] << "  " << m[1][3] << "  " << m[2][3] << "  " << m[3][3] << " ]" << std::endl;
			std::cout << std::endl;
		}
	};
}

#endif;
// Author: Riad Zitouni

#ifndef TEXTURE_H
#define TEXTURE_H

#include <GL\glew.h>
#include <string>

namespace pbrcpp{
	
	enum Type
	{
		DEFAULT, 
		ALBEDO,
		NORMAL,
		ROUGHNESS,
		METALLIC,
		AMBIENT_OCCLUSION,
		HDR
	};

	class Texture{	
	public:
		Texture();
		Texture(const std::string& fullPath, Type type);
		
		inline const GLuint getId() const { return m_id; }
		
		void load();
		void loadHDR();

		static std::string getName(Type type);

	private:
		std::string m_fullPath;
		GLuint m_id;
		Type m_type;
	};
}

#endif
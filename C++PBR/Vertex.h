// Author: Riad Zitouni

#ifndef VERTEX_H
#define VERTEX_H

#include <glm\glm.hpp>

namespace pbrcpp
{
	class Vertex
	{
	public:
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec3 tangent;
		glm::vec2 texCoords;
	};
}

#endif;
// Author: Riad Zitouni

#include "Material.h"

namespace pbrcpp
{
	Material::Material()
	{
		m_albedo = glm::vec3(0.8);
		m_roughness = 0.2;
		m_metallic = 0.2;
		m_ambientOcclusion = 1.0;
	}

	Material::Material(glm::vec3 albedo, float roughness, float metallic, float ambientOcclusion)
	{
		m_albedo = albedo;
		m_roughness = roughness;
		m_metallic = metallic;
		m_ambientOcclusion = ambientOcclusion;
	}

	Material::Material(Texture albedo, Texture normal, Texture roughness, Texture metallic, Texture ambientOcclusion)
	{
		m_albedoMap = albedo;
		m_normalMap = normal;
		m_roughnessMap = roughness;
		m_metallicMap = metallic;
		m_ambientOcclusionMap = ambientOcclusion;
	}
}
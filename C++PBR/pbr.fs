#version 330 core
in vec3 in_position;
in vec3 io_normal;
in vec2 io_texCoords;

out vec4 fragmentColor;

in VertexData
{
	mat3 TBN;
}io_vd;

struct PointLight
{
	vec3 position;
	vec3 color;
};

#define MAX_POINT_LIGHTS 10

uniform PointLight u_pointLights[MAX_POINT_LIGHTS];

uniform vec3 u_viewPosition;

uniform float u_activePointLights;

const float PI = 3.14159265359;

float DistributionGGX(vec3 N, vec3 H, float roughness);
float GeometrySchlickGGX(float NdotV, float roughness);
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness);
vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness);

vec3 processPointLights(vec3 albedo, float roughness, float metallic, vec3 V, vec3 N, vec3 F0);

uniform samplerCube u_irradianceMap;
uniform samplerCube u_prefilteredMap;
uniform sampler2D u_brdfLUT;

uniform float u_albedoID;
uniform float u_normalID; // Normal map
uniform float u_roughnessID;
uniform float u_metallicID;
uniform float u_aoID; // Amient Occlusion

uniform vec3 u_albedo;
uniform float u_roughness;
uniform float u_metallic;
uniform float u_ao;

uniform sampler2D u_textures[29];

void main()
{
	vec3 normal;	
	
	// Texture id
	int tidi = 0;

	// Do normal mapping
	tidi = int(u_normalID); 
	if(tidi > 0)
	{
		
		normal = texture(u_textures[tidi], io_texCoords).rgb;
		normal = normalize(normal * 2.0 - 1.0);
		normal = normalize(io_vd.TBN * normal);
	}
	else{
		normal = io_normal;
	}

	vec3 viewDirection = normalize(u_viewPosition - in_position);
	vec3 R = reflect(-viewDirection, normal);

	vec3 albedo = u_albedo;
	float roughness = u_roughness;
	float metallic = u_metallic;
	float ao = u_ao;

	tidi = int(u_albedoID);
	if (tidi > 0)
	{
		albedo = pow(texture(u_textures[tidi], io_texCoords).rgb, vec3(2.2));
	}

	tidi = int(u_roughnessID);
	if (tidi > 0)
	{
		roughness = texture(u_textures[tidi], io_texCoords).r;
	}

	tidi = int(u_metallicID);
	if (tidi > 0)
	{
		metallic = texture(u_textures[tidi], io_texCoords).r;
	}

	tidi = int(u_aoID);
	if (tidi > 0)
	{
		ao = texture(u_textures[tidi], io_texCoords).r;
	}

	// Surface reflection at 0 incidence angle (when looking directly into at surface).  
	vec3 F0 = vec3(0.04);
	F0 = mix(F0, albedo, metallic);
	
	vec3 Lo = processPointLights(albedo, roughness, metallic, viewDirection, normal, F0);

	// Ambient light
	vec3 F = fresnelSchlickRoughness(max(dot(normal, viewDirection), 0.0), F0, roughness);
	vec3 Kd = 1.0 - F;
	Kd *= 1.0 - metallic;	  

	vec3 irradiance = texture(u_irradianceMap, normal).rgb;
	
	vec3 diffuse = irradiance * albedo;

	// Sample pre-filtered map and brdf LUT to compute specular (split-sum pproximation)
	const float MAX_SPECULAR_LOD = 5.0;
	vec3 prefilteredColor = textureLod(u_prefilteredMap, R, roughness * MAX_SPECULAR_LOD).rgb;
	vec2 brdf = texture(u_brdfLUT, vec2(max(dot(normal, viewDirection), 0.0), roughness)).rg;
	vec3 specular = prefilteredColor * (F * brdf.x + brdf.y);

	vec3 ambient = (Kd * diffuse + specular) * ao;

	vec3 color = ambient + Lo;

	// HDR tone mapping
	color = color / (color + vec3(1.0));

	// Gamma correction
	color = pow(color, vec3(1.0 / 2.2));

	fragmentColor = vec4(color, 1.0);
}

vec3 processPointLights(vec3 albedo, float roughness, float metallic, vec3 V, vec3 N, vec3 F0)
{
	vec3 Lo = vec3(0.0);
	
	for(int i = 0; i < int(u_activePointLights); i++)
	{
		PointLight light = u_pointLights[i];
		vec3 lightPosition = light.position;

		// Calculate light radiance

		// Light direction
		vec3 L = normalize(lightPosition - in_position);

		// Half way vector
		vec3 H = normalize(V + L);

		// Light attenuation over distance travelled
		float distance = length(L);
		float attenuation = 1.0 / distance * distance;
		vec3 radiance = light.color * attenuation;

		// Calculate BRDF using the Cook-Torrance BRDF
		// BRDF approximates the contribution of each light ray to the final reflected light 

		// Normalized Distribution Function. Approximates the amount of microfacets that are aligned with the half-way vector
		float NDF = DistributionGGX(N, H, roughness);

		// Geometry equation. Approximates the amount of overshadowed microfacets
		float G = GeometrySmith(N, V, L, roughness);

		// Fresnel equation. Describes the ratio of reflections
	    vec3 F = fresnelSchlickRoughness(max(dot(H, V), 0.0), F0, roughness);

		vec3 nominator = NDF * G * F;

		// Add 0.001 to avoid division by 0
		float denominator = 4 * max(dot(V, N), 0.0) * max(dot(L, N), 0.0) + 0.001;
		vec3 brdf = nominator / denominator;

		// F here replaces Ks
		vec3 kd = vec3(1.0) - F;

		// The more metallic a material is, the smaller the refraction factor  
		kd *= 1.0 - metallic;

		// Outgoing radiance Lo
		float NdotL = max(dot(N, L), 0.0);

		// The outgoing rdiance (result of reflectance equation integration over the hemisphere)
		// BRDF is not multiplied by Ks here because we already multiplied brdf with F above and we are seeting Ks equal to F
		Lo += ((kd * albedo / PI) + brdf) * radiance * NdotL;
	}
	
	return Lo;
}

// Calculates the probability of microfacets the are aligned with some half-way vector
// Example: if 70% of microfacets are aligned with some half-way vector, then this function return 0.70
// This function uses the Trowbridge-Reits GGX normal distibution function
// alpha^2 / PI * ((n dot h)^2 * (alpha^2 - 1) + 1)^2
// alpha = roughness
// h = half-way vector (vector between light and view vectors)
float DistributionGGX(vec3 N, vec3 H, float roughness)
{
	float roughness4 = pow(roughness, 4);

	float NdotH = max(dot(N, H), 0.0);
	float NdotHSquared = NdotH * NdotH;

	float denominator = (NdotHSquared * (roughness4 - 1.0) + 1.0);
	denominator = PI * denominator * denominator;

	return roughness4 / denominator;
}

// Calculates the distribution of microfacets that are overshdowed
// This function uses the Schlick-GGX function
// k = (roughness^2 + 1)^2 / 8 for direct lights
// k = roughness^2 / 2 for IBL
float GeometrySchlickGGX(float dotProduct, float roughness)
{
	float k = ((roughness + 1.0) * (roughness + 1.0)) / 8.0;

	float nominator = dotProduct;
	float denominator = dotProduct * (1.0 - k) + k;

	return nominator / denominator;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
	float ggx2 = GeometrySchlickGGX(max(dot(N, V), 0.0), roughness);
	float ggx1 = GeometrySchlickGGX(max(dot(N, L), 0.0), roughness);

	return ggx1 * ggx2;
}

// Calculates the ratio of light that gets reflected
// This function uses the Fresnel-Schlick pproximation
vec3 fresnelSchlickRoughness(float NdotV, vec3 F0, float roughness)
{
	return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - NdotV, 5.0);
}
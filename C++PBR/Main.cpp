// Author: Riad Zitouni

#include "Scene.h"

#include "Window.h"

#include "RenderShader.h"

#include "HDRSkyBox.h"
#include "Model.h"

#include "Input.h"
#include "Timer.h"
#include "Light.h"
#include "Renderer.h"

#include "FileUtils.h"
#include "GraphicUtils.h"

#include "MathUtils.h"

#include <glm/gtc/type_ptr.hpp>

#include <Windows.h>

using namespace pbrcpp;

void setOpenGLParameters();
void initShaders();
void initDefaultDrawables();
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
void mouseCallback(GLFWwindow* window, double xpos, double ypos);
void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);

Window window("Default Window", 1024, 576);

Renderer renderer;

Input input;
RenderShader* renderShader;
RenderShader* equiEnvMapShader;

Scene* scene = NULL;

double frameTime = 0;

double lastX = 0.0;
double lastY = 0.0;

bool isMWHPressed = false;

std::string folderPath;

void setOpenGLParameters()
{
	glfwSetKeyCallback(window.getWindow(), keyCallback);
	glfwSetCursorPosCallback(window.getWindow(), mouseCallback);
	glfwSetMouseButtonCallback(window.getWindow(), mouseButtonCallback);
}

void initShaders()
{
	scene->addPointLight(Light(FileUtils::combine(folderPath, "Mesh", "Cube\\cube.obj"), glm::vec3(2.0), glm::vec3(0, 0, 10)));
}

void initDefaultDrawables()
{
	Mesh::MeshData data = MeshLoader::load(FileUtils::combine(folderPath, "Mesh", "smooth_sphere_textured\\smooth_sphere_textured.obj"));
	Mesh* mesh = new Mesh(data, glm::vec3(10.0, 0.0, 0), glm::vec3(5, 5, 5));

	data = MeshLoader::load(FileUtils::combine(folderPath, "Mesh", "smooth_sphere\\smooth_sphere.obj"));
	data.material.setMaterial(glm::vec3(0.1, 0.0, 0.8), 0.1, 0.8, 1.0);

	Mesh* mesh2 = new Mesh(data, glm::vec3(0.0, 0.0, 0), glm::vec3(5, 5, 5));

	data = MeshLoader::load(FileUtils::combine(folderPath, "Mesh", "smooth_sphere\\smooth_sphere.obj"));
	data.material.setMaterial(glm::vec3(0.1, 0.8, 0.1), 0.9, 0.2, 1.0);

	Mesh* mesh3 = new Mesh(data, glm::vec3(-10.0, 0.0, 0), glm::vec3(5, 5, 5));

	data = MeshLoader::load(FileUtils::combine(folderPath, "Mesh", "smooth_sphere\\smooth_sphere.obj"));
	data.material.setMaterial(glm::vec3(0.5, 0.5, 0.8), 0.1, 0.8, 1.0);
	Mesh* mesh4 = new Mesh(data, glm::vec3(0.0, 10.0, 0.0), glm::vec3(5, 5, 5));

	scene->addMesh(mesh);
	scene->addMesh(mesh2);
	scene->addMesh(mesh3);
	scene->addMesh(mesh4);
}

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	Window* instance = (Window*)glfwGetWindowUserPointer(window);

	// If escape key pressed, take proper action
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

	// Check keys other than escape key
	if (key >= 0 && key < MAX_KEYS)
	{
		if (action == GLFW_PRESS)
		{
			input.press(key);
		}
		else if (action == GLFW_RELEASE)
		{
			input.release(key);
		}
	}
}

void updateMouseValues(double x, double y)
{
	if (Input::isLMBPressed() || isMWHPressed)
	{
		GraphicUtils::setMouseXIntensity(x - lastX);
		GraphicUtils::setMouseYIntensity(lastY - y);

		if (Input::isLMBPressed())
		{
			scene->m_meshes[0]->setRotation(GraphicUtils::m_mouseXIntensity, glm::vec3(0.0, 1.0, 0.0), frameTime);
			scene->m_meshes[0]->setRotation(-GraphicUtils::m_mouseYIntensity, glm::vec3(1.0, 0.0, 0.0), frameTime);
		}
	}


	GraphicUtils::setMouseCoords(x, y);

	lastX = x;
	lastY = y;
}

void mouseCallback(GLFWwindow* window, double x, double y)
{
	updateMouseValues(x, y);
}

void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		Input::pressLMB(true);
	}
	else if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_PRESS)
	{
		isMWHPressed = true;
	}
	else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
	{
		Input::pressLMB(false);
		GraphicUtils::setMouseXIntensity(0.0f);
		GraphicUtils::setMouseYIntensity(0.0f);
	}
	else if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_RELEASE)
	{
		isMWHPressed = false;
	}
}

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow){

	char pathBuffer[1024];
	GetModuleFileName(NULL, pathBuffer, 1024);
	std::string::size_type pathEndIndex = std::string(pathBuffer).find("C++PBR");
	folderPath = FileUtils::combine(std::string(pathBuffer).substr(0, pathEndIndex + 6), "C++PBR").c_str();
	
	scene = new Scene("pbr.vs", "pbr.fs");

	renderShader = scene->getShader();
	equiEnvMapShader = new RenderShader("equilateral_envmap.vs", "equilateral_envmap.fs");

	HDRSkyBox hdrSkyBox("newport_loft.hdr", scene->getShader()->getProjectionMatrix(), equiEnvMapShader);
	renderShader->use();

	setOpenGLParameters();

	initDefaultDrawables();
	
	initShaders();

	renderer.load();

	Timer timer;
	timer.start();

	Shader brdfShder("brdf.vs", "brdf.fs");

	Mesh* lightMesh = scene->m_pointLights[0].getMesh();

	float angle = 0.0;

	while (!window.close()){
		timer.setFrameTime();

		timer.getFPS();

		frameTime = Timer::m_frameTime;

		window.clearBuffers();

		if (isMWHPressed){
			scene->getCamera().setPitchYaw(GraphicUtils::m_mouseX, GraphicUtils::m_mouseY, GraphicUtils::m_mouseXIntensity, GraphicUtils::m_mouseYIntensity, frameTime);
		}

		renderShader->use();
		renderShader->setViewMatrix(scene->getCamera().getViewMatrix());
		renderShader->setViewMatrixPosition(scene->getCamera().getPosition());

		scene->update();

		renderer.draw(renderShader);

		hdrSkyBox.bind(renderShader);
		hdrSkyBox.draw(scene->getCamera().getViewMatrix());

		glDepthFunc(GL_LESS);

		window.update();

		float amplitude = 20.0;
		lightMesh->setTranslation(glm::vec3(sin(angle) * amplitude, 0.0, 0.0), frameTime);

		angle += 0.1;

		if (angle > 360)
		{
			angle = 0;
		}
	}

	delete scene;

	return 0;
}
// Author: Riad Zitouni

#include "Texture.h"
#include "FileUtils.h"

#include "stb_image.h"

namespace pbrcpp{
	Texture::Texture()
	{
		m_id = 0;
	}

	Texture::Texture(const std::string& fullPath, Type type)
	{
		m_fullPath = fullPath;
		m_type = type;

		if (type == Type::HDR)
		{
			loadHDR();
		}
		else
		{
			load();
		}
	}

	void Texture::load()
	{
		int width; 
		int height;
		int components;

		unsigned char* image = stbi_load(m_fullPath.c_str(), &width, &height, &components, 3);

		if (image)
		{
			glGenTextures(1, &m_id);
			glBindTexture(GL_TEXTURE_2D, m_id);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
			glGenerateMipmap(GL_TEXTURE_2D);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glBindTexture(GL_TEXTURE_2D, 0);
			
			stbi_image_free(image);
		}
	}

	void Texture::loadHDR()
	{
		int width;
		int height;
		int components;

		stbi_set_flip_vertically_on_load(true);

		float* image = stbi_loadf(m_fullPath.c_str(), &width, &height, &components, 3);

		if (image)
		{
			glGenTextures(1, &m_id);
			glBindTexture(GL_TEXTURE_2D, m_id);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, image);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glBindTexture(GL_TEXTURE_2D, 0);

			stbi_image_free(image);
		}
	}

	std::string Texture::getName(Type type)
	{
		switch (type)
		{
		case ALBEDO:
			return "albedo";
		case NORMAL:
			return "normal";
		case ROUGHNESS:
			return "roughness";
		case METALLIC:
			return "metallic";
		case AMBIENT_OCCLUSION:
			return "ao";
		}
	}
}
#version 330 core

out vec4 fragmentColor;

in vec3 io_position;

uniform sampler2D u_equilateralMap;

const vec2 invAtan = vec2(0.1591, 0.3183);
vec2 sampleSphericlMap(vec3 v){
	vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
	uv *= invAtan;
	uv += 0.5;
	return uv;
}

void main(){
	vec2 uv = sampleSphericlMap(normalize(io_position));
	vec3 color = texture(u_equilateralMap, uv).rgb;

	fragmentColor = vec4(color, 1.0);
}
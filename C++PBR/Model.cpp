// Author: Riad Zitouni

#include "Model.h"
#include "Mesh.h"
#include "FileUtils.h"
#include "Texture.h"
#include <Windows.h>

#include <iostream>
#include <assimp/Importer.hpp>

namespace pbrcpp{

	MeshLoader::~MeshLoader(){

	}

	// Loads a MeshLoader with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
	Mesh::MeshData MeshLoader::load(std::string path)
	{
		Assimp::Importer importer;
		std::string extension = path.substr(path.find_last_of('.'), path.size());

		if (FileUtils::fileExists(path))
		{
			// Read file via ASSIMP
			const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenNormals | aiProcess_CalcTangentSpace);
			if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
			{
				throw std::exception(importer.GetErrorString());
			}

			// Retrieve the directory path of the filepath
			std::replace(path.begin(), path.end(), '\\', '/');

			// Process ASSIMP's root node recursively
			aiNode* node = scene->mRootNode->mChildren[0];
			aiMesh* mesh = scene->mMeshes[node->mMeshes[0]];

			return processMesh(path, mesh, scene);
		}
		else{
			throw std::exception(std::string("Could not find path: ").append(path).c_str());
		}
	}

	Mesh::MeshData MeshLoader::processMesh(std::string path, aiMesh* mesh, const aiScene* scene)
	{
		// Data to fill
		std::vector<aiFace> faces;
		std::vector<Vertex> vertices;
		std::vector<GLuint> indices;

		// Walk through each of the mesh's vertices
		for (GLuint i = 0; i < mesh->mNumVertices; i++)
		{
			Vertex vertex;
			glm::vec3 vector; 

			// Positions
			vector.x = mesh->mVertices[i].x;
			vector.y = mesh->mVertices[i].y;
			vector.z = mesh->mVertices[i].z;
			vertex.position = vector;
			
			// Normals
			vector.x = mesh->mNormals[i].x;
			vector.y = mesh->mNormals[i].y;
			vector.z = mesh->mNormals[i].z;
			vertex.normal = vector;

			// Tangent for normal mapping
			if (mesh->mTangents != NULL){
				vector.x = mesh->mTangents[i].x;
				vector.y = mesh->mTangents[i].y;
				vector.z = mesh->mTangents[i].z;
				vertex.tangent = vector;
			}

			// Texture Coordinates
			if (mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
			{
				glm::vec2 vec;
				// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
				// use MeshLoaders where a vertex can have multiple texture coordinates so we always take the first set (0).
				vec.x = mesh->mTextureCoords[0][i].x;
				vec.y = mesh->mTextureCoords[0][i].y;
				vertex.texCoords = vec;
			}
			else{
				vertex.texCoords = glm::vec2(0.0f, 0.0f);
			}
			
			vertices.push_back(vertex);
		}

		// Now walk through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
		for (GLuint i = 0; i < mesh->mNumFaces; i++)
		{
			aiFace face = mesh->mFaces[i];

			faces.push_back(face);

			// Retrieve all indices of the face and store them in the indices vector
			for (GLuint j = 0; j < face.mNumIndices; j++){
				indices.push_back(face.mIndices[j]);
			}
		}

		path = path.substr(0, path.find_last_of("/"));

		return Mesh::MeshData(faces, vertices, indices, 
			Material(
				loadTexture(path, Type::ALBEDO), 
				loadTexture(path, Type::NORMAL), 
				loadTexture(path, Type::ROUGHNESS), 
				loadTexture(path, Type::METALLIC), 
				loadTexture(path, Type::AMBIENT_OCCLUSION)));
	}

	Texture MeshLoader::loadTexture(std::string path, Type type)
	{
		std::string textureName = Texture::getName(type).append(".png");
		std::string texturePath = FileUtils::combinefs(path, textureName);

		Texture texture;
		GLboolean skip = false;

		if (!FileUtils::fileExists(texturePath))
		{
			return Texture();
		}
			
		return Texture(texturePath, type);
	}
}
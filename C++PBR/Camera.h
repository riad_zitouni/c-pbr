// Author: Riad Zitouni

#ifndef CAMERA_H
#define CAMERA_H

#include <glm\glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

namespace pbrcpp{

#define MAX_PITCH_ANGLE 89

	enum Direction
	{
		FORWARD,
		BACK,
		LEFT,
		RIGHT,
		VERTICAL
	};

	class Camera{

	public:

		Camera();
		~Camera();

		void update();

		void move(Direction direction, float intensity = 0.0f);
		void setPitchYaw(float x, float y, float xIntensity, float yIntensity, float frameTime);

		glm::mat4 getViewMatrix() const;

		inline const glm::vec3 getPosition() const{ return m_position; }
	private:

		float m_prevX;
		float m_prevY;

		float m_pitchAngle;
		float m_yawAngle;

		float m_sensitivity;

		glm::vec3 m_position;
		glm::vec3 m_lookAt;
		glm::vec3 m_direction;
		glm::vec3 m_up;
		glm::vec3 m_right;

		glm::quat m_rotation;
	};

}

#endif
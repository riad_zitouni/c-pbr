// Author: Riad Zitouni

#include "Input.h"

namespace pbrcpp
{

	bool Input::m_keys[MAX_KEYS];
	bool Input::m_isLMBPressed = false;

	void Input::press(unsigned int keyCode)
	{
		if (keyCode < MAX_KEYS)
		{
			m_keys[keyCode] = true;
		}
	}

	void Input::release(unsigned int keyCode)
	{
		if (keyCode < MAX_KEYS)
		{
			m_keys[keyCode] = false;
		}
	}

	const bool Input::isKeyPressed(unsigned int keyCode)
	{
		if (keyCode < MAX_KEYS)
		{
			return m_keys[keyCode];
		}
	}

}
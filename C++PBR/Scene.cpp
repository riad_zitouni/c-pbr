// Author: Riad Zitouni

#include "Scene.h"
#include <sstream>

#include "Input.h"
#include "GraphicUtils.h"

#include <GLFW\glfw3.h>

namespace pbrcpp
{
	std::vector<Mesh*> Scene::m_meshes;
	std::vector<Light> Scene::m_pointLights;

	Scene::Scene(const char* vertexShaderPath, const char* fragmentShaderPath)
	{
		m_shader = new RenderShader(vertexShaderPath, fragmentShaderPath);
	}

	Scene::~Scene()
	{
		for (Mesh* mesh : m_meshes)
		{
			delete mesh;
		}
		m_meshes.clear();

		delete m_shader;
	}

	void Scene::addPointLight(const Light& light)
	{
		m_shader->use();
		
		m_shader->setPointLightShaderData(m_pointLights.size(), light);
		
		m_meshes.push_back(light.getMesh());

		m_pointLights.push_back(light);

		m_shader->uniform1f("u_activePointLights", (float)m_pointLights.size());
	}

	void Scene::addMesh(Mesh* mesh)
	{
		m_meshes.push_back(mesh);
	}

	void Scene::updatePointLights()
	{
		for (int i = 0; i < m_pointLights.size(); i++)
		{
			std::ostringstream positionOss;
			positionOss << "u_pointLights" << "[" << i << "]" << ".position";
			std::string positionString(positionOss.str());

			Light pointLight = m_pointLights[i];

			m_shader->uniform3f(positionString.c_str(), pointLight.getMesh()->getPosition());
			//pointLight.setPosition(m_pointLights[i].getMesh()->getPosition());
		}
	}

	void Scene::moveCamera()
	{
		if (Input::isKeyPressed(GLFW_KEY_W))
		{
			m_camera.move(FORWARD);
		}
		if (Input::isKeyPressed(GLFW_KEY_A))
		{
			m_camera.move(LEFT);
		}
		if (Input::isKeyPressed(GLFW_KEY_S))
		{
			m_camera.move(BACK);
		}
		if (Input::isKeyPressed(GLFW_KEY_D))
		{
			m_camera.move(RIGHT);
		}
		if (Input::isKeyPressed(GLFW_KEY_LEFT_CONTROL) && Input::isLMBPressed())
		{
			m_camera.move(VERTICAL, GraphicUtils::m_mouseYIntensity);
		}
	}

	void Scene::update()
	{
		m_shader->use();
		updatePointLights();
		moveCamera();
		GraphicUtils::setMouseIntensity(0, 0);
	}
}
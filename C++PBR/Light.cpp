// Author: Riad Zitouni

#include "Light.h"
#include "Model.h"

#include <iostream>

namespace pbrcpp
{

	Light::Light()
	{

	}

	Light::Light(std::string path, glm::vec3 color, glm::vec3 position)
	{		
		Mesh::MeshData data = MeshLoader::load(path);
		data.material.setMaterial(color, 0, 0, 1.0);
		m_mesh = new Mesh(data, position, glm::vec3(0.3f, 0.3f, 0.3f));
		
		m_color = color;
	}
}
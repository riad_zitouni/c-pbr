// Author: Riad Zitouni

#include "Window.h"

#include <iostream>

namespace pbrcpp
{

	int Window::m_width;
	int Window::m_height;

	Window::Window()
	{
		m_title = "Default Window";
		m_width = 1024;
		m_height = 576;

		// Instantiate window
		if (!init())
		{
			glfwTerminate();
		}

	}

	Window::Window(const char* title, int width, int height)
	{
		m_title = title;
		m_width = width;
		m_height = height;

		// Instantiate window

		if (!init())
		{
			glfwTerminate();
		}
	}

	Window::~Window()
	{
		glfwTerminate();
	}

	bool Window::init()
	{
		if (!glfwInit())
		{
			return false;
		}

		// Disallow resizing. Must be before creating window
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

		m_window = glfwCreateWindow(m_width, m_height, m_title, NULL, NULL);

		if (!m_window)
		{
			return false;
		}

		glfwMakeContextCurrent(m_window);

		// Set pointer to get window instance in callbacks
		glfwSetWindowUserPointer(m_window, this);

		// Set callbacks
		glfwSetFramebufferSizeCallback(m_window, windowResizeCallback);

		// Init glew
		if (glewInit() != GLEW_OK)
		{
			return false;
		}

		// Set the clear color
		glClearColor(0.3f, 0.4f, 0.4f, 1.0f);

		// Set viewport
		glViewport(0, 0, m_width, m_height);

		// Enable OpenGL options
		glEnable(GL_DEPTH_TEST);

		glEnable(GL_STENCIL_TEST);
		glStencilOp(GL_ZERO, GL_KEEP, GL_REPLACE);

		glEnable(GL_BLEND); 
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Enable linear interpoltion between cube faces to eliminate cubemap edges seams appearing at low resolution mipmaps problem 
		glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

		return true;
	}

	void Window::update()
	{
		// Check fo opengl errors
		GLenum error = glGetError();

		glfwPollEvents();

		glfwSwapBuffers(m_window);
	}

	int Window::close() const
	{
		return glfwWindowShouldClose(m_window) == 1;
	}

	void Window::clearBuffers()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	}

	void windowResizeCallback(GLFWwindow* window, int width, int height)
	{
		Window* instance = (Window*)glfwGetWindowUserPointer(window);
		instance->m_width = width;
		instance->m_height = height;
	}

}


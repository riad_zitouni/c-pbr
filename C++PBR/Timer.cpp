// Author: Riad Zitouni

#include "Timer.h"

#include <windows.h>
#include <iostream>
namespace pbrcpp{
	
	double Timer::m_frameTime = 0.0;

	Timer::Timer(){
		m_countsPerSecond = 0.0;
		m_counterStart = 0.0;
		m_frameCount = 0.0;
		m_fps = 0.0;
		m_frameTimeOld = 0.0;
		m_frameTime = 0.0;

		m_firstStart = true;
	}

	void Timer::start(){
		LARGE_INTEGER frequency;
		QueryPerformanceFrequency(&frequency);

		m_countsPerSecond = double(frequency.QuadPart);

		QueryPerformanceCounter(&frequency);
		m_counterStart = frequency.QuadPart;
	}

	double Timer::getTime(){
		LARGE_INTEGER currentTime;
		QueryPerformanceCounter(&currentTime);
		return double(currentTime.QuadPart - m_counterStart) / m_countsPerSecond;
	}

	void Timer::setFrameTime(){
		LARGE_INTEGER currentTime;
		__int64 tickCount;
		QueryPerformanceCounter(&currentTime);

		if (m_firstStart){
			tickCount = 0.0;
			m_firstStart = false;
		}
		else{
			tickCount = currentTime.QuadPart - m_frameTimeOld;
		}

		m_frameTimeOld = currentTime.QuadPart;

		if (tickCount < 0.0f){
			tickCount = 0.0f;
		}

		m_frameTime = float(tickCount) / m_countsPerSecond;
	}

	double Timer::getFPS(){
		m_frameCount++;
		if (getTime() > 1.0f){
			m_fps = m_frameCount;
			m_frameCount = 0;
			start();
		}

		return m_fps;
	}
}
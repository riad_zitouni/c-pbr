// Author: Riad Zitouni

#ifndef GRAPHIC_UTILS_H
#define GRAPHIC_UTILS_H

namespace pbrcpp
{
	static class GraphicUtils
	{
	public:
		inline static void setMouseCoords(const double x, const double y) { m_mouseX = x; m_mouseY = y; }
		
		inline static void setMouseIntensity(const float x, const float y) 
		{ 
			m_mouseXIntensity = x; 
			m_mouseYIntensity = y; 
		}
		
		inline static void setMouseXIntensity(const float value) { m_mouseXIntensity = value; }
		inline static void setMouseYIntensity(const float value) { m_mouseYIntensity = value; }

		static double m_mouseX;
		static double m_mouseY;

		static float m_mouseXIntensity;
		static float m_mouseYIntensity;
	};
}

#endif
// Author: Riad Zitouni

#include "Mesh.h"

#include "Renderer.h"

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

namespace pbrcpp{

	int Mesh::m_lastId;

	int Mesh::generateId()
	{
		++m_lastId;
		return m_lastId;
	}

	Mesh::Mesh(MeshData data, glm::vec3 position, glm::vec3 scale)
	{
		m_position = position;
		m_scale = scale;
		m_rotationAngle = 0;

		m_modelMatrix = glm::mat4(1.0);
		m_modelMatrix = glm::translate(m_modelMatrix, m_position);
		m_modelMatrix = glm::rotate(m_modelMatrix, 0.0f, glm::vec3(1, 0, 0));
		m_modelMatrix = glm::scale(m_modelMatrix, m_scale);

		m_rotationAxis = glm::vec3(1.0, 0.0, 0.0); // Keep this at 1 so objects don't disappear

		m_objectId = generateId();

		setData(data);
		update();
	}

	void Mesh::setData(MeshData data)
	{
		m_meshData = MeshData(data);
	}

	void Mesh::update()
	{
		m_modelMatrix = glm::mat4();

		glm::mat4 translationMatrix(1.0);
		translationMatrix = glm::translate(translationMatrix, m_position);

		float x = m_rotationAxis.x * sin(m_rotationAngle / 2);
		float y = m_rotationAxis.y * sin(m_rotationAngle / 2);
		float z = m_rotationAxis.z * sin(m_rotationAngle / 2);
		float w = cos(m_rotationAngle / 2);

		glm::quat quaternion;
		quaternion = glm::quat(w, x, y, z);

		glm::mat4 currentRotation = glm::toMat4(quaternion);

		m_rotationMatrix = currentRotation * m_rotationMatrix;

		glm::mat4 scaleMatrix(1.0);
		scaleMatrix = glm::scale(scaleMatrix, m_scale);

		m_modelMatrix = translationMatrix * m_rotationMatrix * scaleMatrix;
	}

	void Mesh::initTransformations(const glm::vec3& position, const glm::vec3& scale)
	{
		m_position = position;
		m_scale = scale;
		m_modelMatrix = glm::mat4(1.0);
		m_modelMatrix = glm::translate(m_modelMatrix, m_position);
		m_modelMatrix = glm::scale(m_modelMatrix, m_scale);
	}

	void Mesh::setTranslation(const glm::vec3& velocity, const float& frameTime)
	{
		m_position += (velocity * frameTime);
		update();
	}

	void Mesh::setRotation(const float& angle, const glm::vec3& axis, const float& frameTime)
	{
		m_rotationAngle = (angle * frameTime);
		m_rotationAxis = axis;
		update();
	}

	void Mesh::setScale(const glm::vec3& size, const float& frameTime)
	{
		m_scale += (size * frameTime);
		update();
	}

	void Mesh::load()
	{
		const std::vector<Vertex>& vertices = m_meshData.vertices;

		glGenBuffers(1, &m_VBO);

		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertices.size(), &vertices[0], GL_STATIC_DRAW);

		const std::vector<GLuint>& indices = m_meshData.indices;

		glGenBuffers(1, &m_IBO);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indices.size(), &indices[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	}
}
// Author: Riad Zitouni

#ifndef RENDER_SHADER
#define RENDER_SHADER

#include "Shader.h"
#include "Light.h"
#include <vector>

namespace pbrcpp{
	class RenderShader : public Shader{

// Max number of point lights allowed in the scene 
#define MAX_POINT_LIGHTS 10

	public:

		RenderShader();
		RenderShader(const char* vertexShaderPath, const char* fragmentShaderPath);

		void setViewMatrix(const glm::mat4& viewMatrix);

		void setViewMatrixPosition(const glm::vec3& position);

		void setPerspectiveMatrixUniform(GLfloat fov, GLfloat aspectRatio, GLfloat near, GLfloat far);

		inline const glm::mat4 getProjectionMatrix() const{ return m_projectionMatrix; }
		
		void setPointLightShaderData(int index, const Light& light);

	private:

		glm::mat4 m_projectionMatrix;

		Light m_pointLights[MAX_POINT_LIGHTS];

		int m_pointLightsSize;

	};
}

#endif
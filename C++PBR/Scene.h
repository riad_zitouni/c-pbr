// Author: Riad Zitouni

#ifndef SCENE_H
#define SCENE_H

#include "Mesh.h"
#include "Light.h"
#include "RenderShader.h"
#include "Camera.h"

namespace pbrcpp
{
	class SceneAsset;
	class Scene
	{

	public:
		struct SelectedObject
		{
			GLuint VAO;
			GLuint VBO;

			Mesh* mesh;

			GLfloat* vertices;

			SelectedObject()
			{
				mesh = NULL;
			}

			SelectedObject(Mesh* _mesh, GLfloat* vertices, GLuint _VAO, GLuint _VBO)
			{
				mesh = _mesh;
				VAO = _VAO;
				VBO = _VBO;
			}
		};

		Scene(const char* vertexShaderPath, const char* fragmentShaderPath);
		~Scene();

		void addPointLight(const Light& light);
		void updatePointLights();

		void update();

		void addMesh(Mesh* mesh);
		
		void moveCamera();

		inline const void setPointLights(const std::vector<Light> lights) { m_pointLights = lights; }
		
		inline RenderShader* getShader() { return m_shader; }

		inline Camera& getCamera() { return m_camera; }

		static std::vector<Mesh*> m_meshes;
		static std::vector<Light> m_pointLights;

		SceneAsset* m_asset;
	private:
		RenderShader* m_shader;
		Camera m_camera;
	};
}

#endif
// Author: Riad Zitouni

#ifndef TIMER_H
#define TIMER_H

namespace pbrcpp
{
	class Timer
	{
		
	public:

		Timer();

		void start();
		double getTime();
		double getFPS();
		
		void setFrameTime();
		
		static double m_frameTime;
	private:
		double m_countsPerSecond;
		__int64 m_counterStart;

		int m_frameCount;
		int m_fps;

		__int64 m_frameTimeOld;
		
		bool m_firstStart;
	};
}

#endif
// Author: Riad Zitouni

#ifndef MESH_H
#define MESH_H

#include "Texture.h"
#include "Vertex.h"
#include "Material.h"

#include <GL\glew.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <vector>

namespace pbrcpp{
	class Mesh
	{		

	public:
		struct MeshData
		{
			std::vector<aiFace> faces;
			std::vector<Vertex> vertices;
			std::vector<GLuint> indices;
			
			Material material;

			MeshData()
			{

			}

			MeshData(std::vector<aiFace> _faces, std::vector<Vertex> _vertices, std::vector<GLuint> _indices, Material _material)
			{
				faces = _faces;
				vertices = _vertices;
				indices = _indices;
				material = _material;
			}

			MeshData(MeshData& data)
			{
				faces = data.faces;
				vertices = data.vertices;
				indices = data.indices;
				material = data.material;
			}
		};

		Mesh(MeshData data, glm::vec3 position, glm::vec3 size);
		Mesh(){}
		
		void setData(const MeshData data);

		void load();

		inline const GLuint getVBO() const { return m_VBO; }
		inline const GLuint getIBO() const { return m_IBO; }

		void initTransformations(const glm::vec3& position, const glm::vec3& scale);

		inline const glm::mat4& getModelMatrix() const { return m_modelMatrix; }

		inline const int& getObjectId() const { return m_objectId; }

		void setTranslation(const glm::vec3& velocity, const float& frameTime);
		void setRotation(const float& angles, const glm::vec3& axis, const float& frameTime);
		void setScale(const glm::vec3& scale, const float& frameTime);

		void update();

		inline const glm::vec3& getPosition() const { return m_position; }
		inline const glm::vec3& getScale() const { return m_scale; }
		inline const glm::vec3& getRotationAxis() const { return m_rotationAxis; }
		inline const float& getRotationAngle() const { return m_rotationAngle; }

		MeshData m_meshData;
	private:
		glm::mat4 m_modelMatrix;

		glm::vec3 m_position;

		glm::vec3 m_rotationAxis;
		float m_rotationAngle;
		glm::mat4 m_rotationMatrix;

		glm::vec3 m_scale;

		int m_objectId;

		int generateId();
		static int m_lastId;

		GLuint m_VBO;
		GLuint m_IBO;
	};
}

#endif
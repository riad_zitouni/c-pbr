// Author: Riad Zitouni

#include "HDRSkyBox.h"

#include "Window.h"
#include "FileUtils.h"
#include <Windows.h>

#include <glm/gtc/type_ptr.hpp>

namespace pbrcpp
{
	// Must take pointer to shader otherwise it renders black 
	HDRSkyBox::HDRSkyBox(const char* textureName, const glm::mat4& projectionMatrix, RenderShader* hdrSkyBoxShader) : m_hdrSkyBoxShader(hdrSkyBoxShader)
	{
		char pathBuffer[1024];
		GetModuleFileName(NULL, pathBuffer, 1024);
		std::string::size_type pathEndIndex = std::string(pathBuffer).find("C++PBR");
		std::string textureFolder = FileUtils::combine(std::string(pathBuffer).substr(0, pathEndIndex + 6), "C++PBR", "Images").c_str();

		m_hdrTexture = Texture(FileUtils::combine(textureFolder, textureName), Type::HDR);
		m_brdfShader = std::make_unique<Shader>("brdf.vs", "brdf.fs");

		initDiffuseIBL();
		initSpecularIBL();

		glViewport(0, 0, Window::getWidth(), Window::getHeight());
	}

	void HDRSkyBox::initDiffuseIBL()
	{
		m_hdrSkyBoxShader->use();
		m_hdrSkyBoxShader->uniform1i("u_environmentMap", 0);

		glGenFramebuffers(1, &m_captureFBO);
		glGenRenderbuffers(1, &m_captureRBO);

		glBindFramebuffer(GL_FRAMEBUFFER, m_captureFBO);
		glBindRenderbuffer(GL_RENDERBUFFER, m_captureRBO);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 512, 512);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_captureRBO);

		glGenTextures(1, &m_envCubemapID);
		glBindTexture(GL_TEXTURE_CUBE_MAP, m_envCubemapID);
		for (unsigned int i = 0; i < 6; i++)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F, 512, 512, 0, GL_RGB, GL_FLOAT, nullptr);
		}

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		m_captureProjection = glm::perspective(glm::radians(90.0f), 1.0f, 0.1f, 10.0f);

		m_captureViews[0] = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f));
		m_captureViews[1] = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f));
		m_captureViews[2] = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		m_captureViews[3] = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f));
		m_captureViews[4] = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, -1.0f, 0.0f));
		m_captureViews[5] = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, -1.0f, 0.0f));

		RenderShader equilateralToCubeMapShader("equilateral_to_cubemap.vs", "equilateral_to_cubemap.fs");
		equilateralToCubeMapShader.use();
		
		equilateralToCubeMapShader.uniform1i("u_equilateralMap", 0);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_hdrTexture.getId());
		
		equilateralToCubeMapShader.uniformMat4("u_projectionMatrix", m_captureProjection);

		glViewport(0, 0, 512, 512);
		glBindFramebuffer(GL_FRAMEBUFFER, m_captureFBO);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		for (unsigned int i = 0; i < 6; i++)
		{
			m_hdrSkyBoxShader->uniformMat4("u_viewMatrix", m_captureViews[i]);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, m_envCubemapID, 0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			renderCube();
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		glBindTexture(GL_TEXTURE_CUBE_MAP, m_envCubemapID);
		glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

		glGenTextures(1, &m_irradianceCubeMapID);
		glBindTexture(GL_TEXTURE_CUBE_MAP, m_irradianceCubeMapID);

		for (unsigned int i = 0; i < 6; i++)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB32F, 32, 32, 0, GL_RGB, GL_FLOAT, nullptr);
		}

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glBindFramebuffer(GL_FRAMEBUFFER, m_captureFBO);
		glBindRenderbuffer(GL_RENDERBUFFER, m_captureRBO);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 32, 32);

		Shader irradianceIntegralShader("sky_box_simple.vs", "irradiance_integral.fs");
		irradianceIntegralShader.use();

		irradianceIntegralShader.uniform1i("u_environmentMap", 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, m_envCubemapID);
		irradianceIntegralShader.uniformMat4("u_projectionMatrix", m_captureProjection);

		glViewport(0, 0, 32, 32);
		glBindFramebuffer(GL_FRAMEBUFFER, m_captureFBO);
		for (unsigned int i = 0; i < 6; i++)
		{
			irradianceIntegralShader.uniformMat4("u_viewMatrix", m_captureViews[i]);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, m_irradianceCubeMapID, 0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			renderCube();
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		glDepthFunc(GL_LESS);
	}

	void HDRSkyBox::initSpecularIBL()
	{
		glGenTextures(1, &m_prefilteredMapID);
		glBindTexture(GL_TEXTURE_CUBE_MAP, m_prefilteredMapID);
		
		for (unsigned int i = 0; i < 6; i++)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F, 128, 128, 0, GL_RGB, GL_FLOAT, nullptr);
		}

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

		RenderShader prefilterShader("sky_box_simple.vs", "prefilter.fs");
		prefilterShader.use();

		prefilterShader.uniform1i("u_environmentMap", 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, m_envCubemapID);
		prefilterShader.uniformMat4("u_projectionMatrix", m_captureProjection);

		glBindFramebuffer(GL_FRAMEBUFFER, m_captureFBO);

		unsigned int maxMipLevels = 5;
		for (unsigned int mip = 0; mip < maxMipLevels; mip++)
		{
			// resize framebuffer to mim-level size
			unsigned int mipWidth = 128 * std::pow(0.5, mip);
			unsigned int mipHeight = 128 * std::pow(0.5, mip);
			glBindRenderbuffer(GL_RENDERBUFFER, m_captureRBO);
			glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, mipWidth, mipHeight);
			glViewport(0, 0, mipWidth, mipHeight);

			float roughness = (float)mip / (float)(maxMipLevels - 1);
			prefilterShader.uniform1f("u_roughness", roughness);

			for (unsigned int i = 0; i < 6; i++)
			{
				prefilterShader.uniformMat4("u_viewMatrix", m_captureViews[i]);
				glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, m_prefilteredMapID, mip);

				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				renderCube();
			}
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		glGenTextures(1, &m_brdfLUTTextureID);

		glBindTexture(GL_TEXTURE_2D, m_brdfLUTTextureID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RG16F, 512, 512, 0, GL_RG, GL_FLOAT, 0);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glBindFramebuffer(GL_FRAMEBUFFER, m_captureFBO);
		glBindRenderbuffer(GL_RENDERBUFFER, m_captureRBO);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 512, 512);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_brdfLUTTextureID, 0);

		glViewport(0, 0, 512, 512);

		m_brdfShader->use();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		renderQuad();

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void HDRSkyBox::bind(RenderShader* shader)
	{
		shader->uniform1i("u_irradianceMap", m_irradianceCubeMapID);
		shader->uniform1i("u_prefilteredMap", m_prefilteredMapID);
		shader->uniform1i("u_brdfLUT", m_brdfLUTTextureID);

		glActiveTexture(GL_TEXTURE0 + m_irradianceCubeMapID);
		glBindTexture(GL_TEXTURE_CUBE_MAP, m_irradianceCubeMapID);

		glActiveTexture(GL_TEXTURE0 + m_prefilteredMapID);
		glBindTexture(GL_TEXTURE_CUBE_MAP, m_prefilteredMapID);

		glActiveTexture(GL_TEXTURE0 + m_brdfLUTTextureID);
		glBindTexture(GL_TEXTURE_2D, m_brdfLUTTextureID);
	}

	void HDRSkyBox::draw(glm::mat4& viewMatrix)
	{
		glDepthFunc(GL_LEQUAL);

		m_hdrSkyBoxShader->use();
		m_hdrSkyBoxShader->uniformMat4("u_viewMatrix", viewMatrix);
		
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, m_envCubemapID);
		renderCube();

		glDepthFunc(GL_LESS);
	}

	void HDRSkyBox::renderCube()
	{
		if (m_cubeVAO == 0)
		{
			GLfloat vertices[] = 
			{
				// Back face
				-1.0f, -1.0f, -1.0f, // Bottom-left
				1.0f,  1.0f, -1.0f, // top-right
				1.0f, -1.0f, -1.0f, // bottom-right         
				1.0f,  1.0f, -1.0f,  // top-right
				-1.0f, -1.0f, -1.0f,  // bottom-left
				-1.0f,  1.0f, -1.0f,// top-left
				
			    // Front face
				-1.0f, -1.0f,  1.0f, // bottom-left
				1.0f, -1.0f,  1.0f,  // bottom-right
				1.0f,  1.0f,  1.0f,  // top-right
				1.0f,  1.0f,  1.0f, // top-right
				-1.0f,  1.0f,  1.0f,  // top-left
				
				// Left face
				-1.0f, -1.0f,  1.0f,  // bottom-left 
				-1.0f,  1.0f,  1.0f, // top-right
				-1.0f,  1.0f, -1.0f, // top-left
				-1.0f, -1.0f, -1.0f,  // bottom-left
				-1.0f, -1.0f, -1.0f, // bottom-left
				-1.0f, -1.0f,  1.0f,  // bottom-right
				-1.0f,  1.0f,  1.0f, // top-right

				// Right face
				1.0f,  1.0f,  1.0f,// top-left
				1.0f, -1.0f, -1.0f,// bottom-right
				1.0f,  1.0f, -1.0f,// top-right         
				1.0f, -1.0f, -1.0f, // bottom-right
			    1.0f,  1.0f,  1.0f, // top-left
			    1.0f, -1.0f,  1.0f,// bottom-left     
																																// Bottom face
			    -1.0f, -1.0f, -1.0f, // top-right
				1.0f, -1.0f, -1.0f,  // top-left
			    1.0f, -1.0f,  1.0f, // bottom-left
			    1.0f, -1.0f,  1.0f,  // bottom-left
				-1.0f, -1.0f,  1.0f, // bottom-right
			    -1.0f, -1.0f, -1.0f, // top-right
																			
				// Top face
				-1.0f,  1.0f, -1.0f,// top-left
			    1.0f,  1.0f , 1.0f, // bottom-right
				1.0f,  1.0f, -1.0f, // top-right     
				1.0f,  1.0f,  1.0f, // bottom-right
				-1.0f,  1.0f, -1.0f,// top-left
				-1.0f,  1.0f,  1.0f// bottom-left        
			};

			glGenVertexArrays(1, &m_cubeVAO);
			glGenBuffers(1, &m_cubeVBO);

			glBindBuffer(GL_ARRAY_BUFFER, m_cubeVBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

			glBindVertexArray(m_cubeVAO);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
		}

		glBindVertexArray(m_cubeVAO);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glBindVertexArray(0);
	}

	void HDRSkyBox::renderQuad()
	{
		if (m_quadVAO == 0)
		{
			GLfloat quadVertices[] = 
			{
				-1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
				-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
				1.0f, 1.0f, 0.0f, 1.0f, 1.0f,
				1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
			};

			glGenVertexArrays(1, &m_quadVAO);
			glGenBuffers(1, &m_quadVBO);
			glBindVertexArray(m_quadVAO);
			glBindBuffer(GL_ARRAY_BUFFER, m_quadVBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
		}
		glBindVertexArray(m_quadVAO);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		glBindVertexArray(0);
	}

}
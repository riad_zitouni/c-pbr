// Author: Riad Zitouni

#include "RenderShader.h"
#include "Window.h"

#include <sstream>

namespace pbrcpp{

	RenderShader::RenderShader()
	{

	}

	RenderShader::RenderShader(const char* vertexShaderPath, const char* fragmentShaderPath) : Shader(vertexShaderPath, fragmentShaderPath)
	{
		use();

		GLint texIDs[31];
		for (int i = 0; i < 31; i++)
		{
			texIDs[i] = i;
		}
		uniform1iv("u_textures", 31, texIDs);

		setPerspectiveMatrixUniform(45.0f, Window::getAspectRatio(), 0.1f, 100.0f);
	}

	void RenderShader::setPointLightShaderData(int index, const Light& light)
	{
		std::ostringstream colorStream;
		std::ostringstream positionStream;

		colorStream << "u_pointLights" << "[" << index << "]" << ".color";
		positionStream << "u_pointLights" << "[" << index << "]" << ".position";

		std::string colorString(colorStream.str());
		std::string positionString(positionStream.str());

		uniform3f(colorString.c_str(), light.getColor());
		uniform3f(positionString.c_str(), light.getMesh()->getPosition());
	}

	void RenderShader::setViewMatrix(const glm::mat4& viewMatrix)
	{
		uniformMat4("u_viewMatrix", viewMatrix);
	}

	void RenderShader::setViewMatrixPosition(const glm::vec3& position)
	{
		uniform3f("u_viewPosition", position);
	}

	void RenderShader::setPerspectiveMatrixUniform(GLfloat fov, GLfloat aspectRatio, GLfloat near, GLfloat far)
	{
		m_projectionMatrix = glm::perspective(fov, aspectRatio, near, far);
		uniformMat4("u_projectionMatrix", m_projectionMatrix);
	}
}
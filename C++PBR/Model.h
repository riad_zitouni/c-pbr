// Author: Riad Zitouni

#ifndef MeshLoader_H
#define MeshLoader_H

#include <assimp/scene.h>
#include <vector>

#include "Texture.h"
#include "Mesh.h"

namespace pbrcpp{
	class MeshLoader{

	public:
		static Mesh::MeshData load(std::string path);

		~MeshLoader();

	private:
		static Mesh::MeshData processMesh(std::string path, aiMesh* mesh, const aiScene* scene);
		
		static Texture loadTexture(std::string path, Type type);
	};
}

#endif
// Author: Riad Zitouni

#ifndef HDR_SKY_BOX
#define HDR_SKY_BOX

#include "RenderShader.h"
#include <memory>

namespace pbrcpp
{
	class HDRSkyBox
	{
	public:

		HDRSkyBox(const char* textureName, const glm::mat4& projectionMatrix, RenderShader* hdrSkyBoxShader);

		void initDiffuseIBL();
		void initSpecularIBL();

		void draw(glm::mat4& viewMatrix);

		void bind(RenderShader* shader);

	private:
		void renderCube();
		void renderQuad();

		GLuint m_cubeVAO;
		GLuint m_cubeVBO;

		GLuint m_quadVAO = 0;
		GLuint m_quadVBO;

		GLuint m_irradianceCubeMapID;

		GLuint m_prefilteredMapID;
		GLuint m_brdfLUTTextureID;

		GLuint m_envCubemapID;

		RenderShader* m_hdrSkyBoxShader;
		Texture m_hdrTexture;

		unsigned int m_captureFBO;
		unsigned int m_captureRBO;
		glm::mat4 m_captureProjection;

		glm::mat4 m_captureViews[6];

		std::unique_ptr<Shader> m_brdfShader;
	};
}

#endif

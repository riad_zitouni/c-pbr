// Author: Riad Zitouni

#include "Renderer.h"
#include "Model.h"
#include "Timer.h"
#include "MathUtils.h"
#include "Scene.h"

#include <iostream>
#include <fstream>

namespace pbrcpp{
	void Renderer::load()
	{
		for (Mesh* mesh : Scene::m_meshes)
		{
			mesh->load();
		}
	}

	void Renderer::draw(RenderShader* shader)
	{
		for (const Mesh* mesh : Scene::m_meshes)
		{
			const Material& material = mesh->m_meshData.material;

			glBindBuffer(GL_ARRAY_BUFFER, mesh->getVBO());
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->getIBO());

			glEnableVertexAttribArray(SHADER_POSITION_LOCATION);
			glEnableVertexAttribArray(SHADER_NORMAL_LOCATION);
			glEnableVertexAttribArray(SHADER_UV_LOCATION);
			glEnableVertexAttribArray(SHADER_TANGENT_LOCATION);

			glVertexAttribPointer(SHADER_POSITION_LOCATION, 3, GL_FLOAT, GL_FALSE, VERTEX_SIZE, (void*)0);

			glVertexAttribPointer(SHADER_NORMAL_LOCATION, 3, GL_FLOAT, GL_FALSE, VERTEX_SIZE, (const GLvoid*)offsetof(Vertex, Vertex::normal));

			glVertexAttribPointer(SHADER_UV_LOCATION, 2, GL_FLOAT, GL_FALSE, VERTEX_SIZE, (const GLvoid*)offsetof(Vertex, Vertex::texCoords));

			glVertexAttribPointer(SHADER_TANGENT_LOCATION, 3, GL_FLOAT, GL_FALSE, VERTEX_SIZE, (const GLvoid*)offsetof(Vertex, Vertex::tangent));

			shader->uniformMat4("u_modelMatrix", mesh->getModelMatrix());

			GLuint albedoID = material.getAlbedoMap().getId();
			GLuint normalID = material.getNormalMap().getId();
			GLuint roughnessID = material.getRoughnessMap().getId();
			GLuint metallicID = material.getMetallicMap().getId();
			GLuint aoID = material.getAmbientOcclusionMap().getId();

			shader->uniform3f("u_albedo", material.getAlbedo());
			shader->uniform1f("u_roughness", material.getRoughness());
			shader->uniform1f("u_metallic", material.getMetallic());
			shader->uniform1f("u_ao", material.getAmbientOcclusion());

			shader->uniform1f("u_normalID", (float)normalID);

			shader->uniform1f("u_albedoID", (float)albedoID);
			shader->uniform1f("u_roughnessID", (float)roughnessID);
			shader->uniform1f("u_metallicID", (float)metallicID);
			shader->uniform1f("u_aoID", (float)aoID);

			glActiveTexture(GL_TEXTURE0 + albedoID);
			glBindTexture(GL_TEXTURE_2D, albedoID);

			glActiveTexture(GL_TEXTURE0 + normalID);
			glBindTexture(GL_TEXTURE_2D, normalID);

			glActiveTexture(GL_TEXTURE0 + roughnessID);
			glBindTexture(GL_TEXTURE_2D, roughnessID);

			glActiveTexture(GL_TEXTURE0 + metallicID);
			glBindTexture(GL_TEXTURE_2D, metallicID);

			glActiveTexture(GL_TEXTURE0 + aoID);
			glBindTexture(GL_TEXTURE_2D, aoID);

			glDrawElements(GL_TRIANGLES, mesh->m_meshData.indices.size(), GL_UNSIGNED_INT, 0);
		}
		
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDisableVertexAttribArray(0);
	}
}

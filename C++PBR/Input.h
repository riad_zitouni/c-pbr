// Author: Riad Zitouni

#ifndef INPUT_H
#define INPUT_H

namespace pbrcpp{

#define MAX_KEYS 1024

	static class Input
	{

	public:

		static void press(unsigned int keyCode);
		static void release(unsigned int keyCode);

		static const bool isKeyPressed(unsigned int keyCode);
		
		inline static const bool isLMBPressed() { return m_isLMBPressed; }
		inline static void pressLMB(bool value) { m_isLMBPressed = value; }

	private:
		static bool m_keys[MAX_KEYS];
		static bool m_isLMBPressed;
	};
}

#endif
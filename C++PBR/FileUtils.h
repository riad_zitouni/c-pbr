// Author: Riad Zitouni

#ifndef FILE_UTILS_H
#define FILE_UTILS_H

#include <iostream>
#include <string>

namespace pbrcpp{

	static class FileUtils
	{
	public:
		// Read non-image files
		static bool readFile(const char* path, std::string& output)
		{
			FILE* file = fopen(path, "rt");
			int result = 0;

			if (file == NULL)
			{
				return false;
			}

			// Get file size
			result = fseek(file, 0, SEEK_END);

			if (result != 0)
			{
				return false;
			}

			unsigned long size = ftell(file);
			size++; // To allocate space for termination character

			char* data = new char[size];

			// Remove unwanted chars
			memset(data, 0, size);

			// Go to begining of file
			result = fseek(file, 0, SEEK_SET);

			if (result != 0)
			{
				return false;
			}

			// Read file
			result = fread(data, 1, size - 1, file);

			// Close file
			fclose(file);

			output = std::string(data);
			delete[] data;

			return true;
		}

		static bool fileExists(std::string path)
		{
			FILE* file = fopen(path.c_str(), "r");
			if (file)
			{
				fclose(file);
				return true;
			}
			else
			{
				return false;
			}
		}

		static std::string getFileNameWithoutExtension(const std::string& path, bool forwardSlash = true)
		{
			std::string name(path);

			int start = name.find_last_of('/') + 1;
			int end = name.find_last_of('.');

			name = name.substr(0, name.find_last_of("."));
			name = forwardSlash ? name.substr(name.find_last_of('/') + 1, name.size()) : name.substr(name.find_last_of('\\') + 1, name.size());
			return name;
		}

		static std::string combine(const std::string& path, const std::string& extra1, const std::string& extra2)
		{
			return std::string(path).append("\\").append(extra1).append("\\").append(extra2);
		}

		static std::string combine(const std::string& path, const std::string& extra)
		{
			return std::string(path).append("\\").append(extra);
		}

		static std::string combinefs(const std::string& path, const std::string& extra)
		{
			return std::string(path).append("/").append(extra);
		}
	};
}

#endif
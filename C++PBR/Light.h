// Author: Riad Zitouni

#ifndef LIGHT_H
#define LIGHT_H

#include "Mesh.h"

namespace pbrcpp
{

	class Light
	{

	public:
		Light();
		
		Light(std::string path, glm::vec3 color, glm::vec3 position);

		inline Mesh* getMesh() const { return m_mesh; }

		inline const glm::vec3 getColor() const { return m_color; }

	private:
		glm::vec3 m_color;
		Mesh* m_mesh;

	};

}

#endif;
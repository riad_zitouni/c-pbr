// Author: Riad Zitouni

#ifndef SHADER_H
#define SHADER_H

#include <GL\glew.h>
#include <glm/gtc/matrix_transform.hpp>

namespace pbrcpp{
	class Shader{

	public:

		Shader();
		Shader(const char* vertexShaderPath, const char* fragmentShaderPath);
		~Shader();

		void use();

		void uniform1i(const GLchar* uniformName, const int value);

		void uniform1f(const GLchar* uniformName, const float value);
		void uniform2f(const GLchar* uniformName, const glm::vec2& value);
		void uniform3f(const GLchar* uniformName, const glm::vec3& value);
		void uniform4f(const GLchar* uniformName, const glm::vec4& value);

		void uniform1iv(const GLchar* uniformName, int size, int* value);

		void uniformMat4(const GLchar* uniformName, const glm::mat4& value);

		GLuint m_program;

		const char* m_vertexShaderPath;
		const char* m_fragmentShaderPath;
	private:
		bool compileShader();
		GLuint getUniform(const char* uniformName);
	};
}

#endif
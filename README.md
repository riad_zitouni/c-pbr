**Summary**

This is my physically based rendering engine personal project. This engine supports both direct and indirect lighting. Direct lighting comes from point lights (like the oscillating cube in the app) and indirect lighting comes from an HDR cube map which can also be seen in the app.

**How to use this app?**

* Run the executable (located in the Release folder in the root folder) or run the project from Visual Studio
* Use WASD keys to move around
* Hold down the middle mouse button and move the mouse to rotate the camera
* Hold down the left mouse button and move the mouse to rotate the textured sphere

-**Acknowledgements**

-The PBR math and algorithms were inspired from https://learnopengl.com/#!PBR/Theory
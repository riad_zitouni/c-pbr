#version 330 core

out vec4 pixelColor;

in vec3 io_position;

uniform samplerCube u_environmentMap;

void main()
{		
    vec3 envColor = textureLod(u_environmentMap, io_position, 1.2).rgb;

    // HDR tonemap and gamma correct
    envColor = envColor / (envColor + vec3(1.0));
    envColor = pow(envColor, vec3(1.0/2.2)); 
    
    pixelColor = vec4(envColor, 1.0);
}
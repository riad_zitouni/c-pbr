#version 330 core

in vec2 io_texCoords;

out vec4 fragmentColor;

const float PI = 3.14159265359;

float geometrySchlickGGX(float dotProduct, float roughness)
{
	float k = (roughness * roughness) / 2.0;

	float nominator = dotProduct;
	float denominator = dotProduct * (1.0 - k) + k;

	return nominator / denominator;
}

float geometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
	float ggx2 = geometrySchlickGGX(max(dot(N, V), 0.0), roughness);
	float ggx1 = geometrySchlickGGX(max(dot(N, L), 0.0), roughness);

	return ggx1 * ggx2;
}

vec3 importanceSampleGGX(vec2 Xi, vec3 normal, float roughness)
{
	float roughnessSquared = roughness*roughness;
	
	float phi = 2.0 * PI * Xi.x;
	float cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (roughnessSquared*roughnessSquared - 1.0) * Xi.y));
	float sinTheta = sqrt(1.0 - cosTheta*cosTheta);
	
	vec3 halfWayVector;
	halfWayVector.x = cos(phi) * sinTheta;
	halfWayVector.y = sin(phi) * sinTheta;
	halfWayVector.z = cosTheta;
	
	vec3 up        = abs(normal.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
	vec3 tangent   = normalize(cross(up, normal));
	vec3 bitangent = cross(normal, tangent);
	
	vec3 sampleVector = tangent * halfWayVector.x + bitangent * halfWayVector.y + normal * halfWayVector.z;

	return normalize(sampleVector);
}

float radicalInverse_VdC(uint bits) 
{
     bits = (bits << 16u) | (bits >> 16u);
     bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
     bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
     bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
     bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
     return float(bits) * 2.3283064365386963e-10;
}

vec2 hammersley(uint i, uint N)
{
	return vec2(float(i)/float(N), radicalInverse_VdC(i));
}

vec2 integrateBRDF(float NdotV, float roughness)
{
	vec3 V;
	V.x = sqrt(1.0 - NdotV * NdotV);
	V.y = 0.0;
	V.z = NdotV;

	float A= 0.0;
	float B = 0.0;

	vec3 N = vec3(0.0, 0.0, 1.0);

	const uint SAMPLE_COUNT = 1024u;
	for(uint i = 0u; i < SAMPLE_COUNT; i++)
	{
		// Generate biased sample
		vec2 Xi = hammersley(i, SAMPLE_COUNT);
		vec3 H = importanceSampleGGX(Xi, N, roughness);
		vec3 L = normalize(2.0 * dot(V, H) * H - V);

		float NdotL = max(L.z, 0.0);
		float NdotH = max(H.z, 0.0);
		float VdotH = max(dot(V, H), 0.0);

		if (NdotL > 0.0)
		{
			float G = geometrySmith(N, V, L, roughness);
			float G_Vis = (G * VdotH) / (NdotH * NdotV);
			float Fc = pow(1.0 - VdotH, 5.0);

			A += (1.0 - Fc) * G_Vis;
			B += Fc * G_Vis;
		}
	}

	A /= float(SAMPLE_COUNT);
	B /= float(SAMPLE_COUNT);

	return vec2(A, B);
}

void main()
{
	vec2 brdf = integrateBRDF(io_texCoords.x, io_texCoords.y);
	fragmentColor = vec4(brdf.x, brdf.y, 0.0, 1.0);
}